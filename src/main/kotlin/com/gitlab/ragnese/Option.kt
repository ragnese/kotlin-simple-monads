@file:Suppress("unused")

package com.gitlab.ragnese

sealed class Option<out T> {
    fun isSome(): Boolean = when (this) {
        is Some<T> -> true
        else -> false
    }

    fun isNone(): Boolean = when (this) {
        is Some<T> -> false
        else -> true
    }

    fun get(): T? = when (this) {
        is Some<T> -> value
        else -> null
    }

    inline fun <V> fold(ifSome: (T) -> V, ifNone: () -> V): V = when (this) {
        is Some<T> -> ifSome(value)
        is None -> ifNone()
    }

    inline fun <V> fold(block: (T?) -> V): V = block(get())

    inline fun <V> map(transformation: (T) -> V): Option<V> = when (this) {
        is Some<T> -> Some(transformation(value))
        is None -> this
    }
}

data class Some<out T>(val value: T) : Option<T>()
object None : Option<Nothing>()

fun <T> T.some(): Option<T> = Some(this)

fun <T> T?.option(): Option<T> = when (this) {
    null -> None
    else -> Some(this)
}

fun <T> Option<T>.getOr(default: T): T = when (this) {
    is Some<T> -> value
    is None -> default
}

inline fun <T> Option<T>.getOrElse(block: () -> T): T = when (this) {
    is Some<T> -> value
    is None -> block()
}

inline fun <T, U> Option<T>.flatMap(transformation: (T) -> Option<U>): Option<U> = when (this) {
    is Some<T> -> transformation(value)
    is None -> this
}

fun <T, E: Throwable> Option<T>.okOr(err: E): Try<T, E> = when (this) {
    is Some<T> -> Success(value)
    is None -> Failure(err)
}

fun <T, E: Throwable> Option<T>.okOrElse(block: () -> E): Try<T, E> = when (this) {
    is Some<T> -> Success(value)
    is None -> Failure(block())
}
