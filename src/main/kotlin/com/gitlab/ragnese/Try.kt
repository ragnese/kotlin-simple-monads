@file:Suppress("unused")

package com.gitlab.ragnese

sealed class Try<out T, out E : Throwable> {
    fun isSuccess(): Boolean = when (this) {
        is Success<T> -> true
        else -> false
    }

    fun isFailure(): Boolean = when (this) {
        is Success<T> -> false
        else -> true
    }

    fun getOrNull(): T? = when (this) {
        is Success<T> -> value
        else -> null
    }

    fun exceptionOrNull(): E? = when (this) {
        is Failure<E> -> error
        else -> null
    }

    fun getOrThrow(): T = when (this) {
        is Success<T> -> value
        is Failure<E> -> throw error
    }

    inline fun <U> map(transformation: (T) -> U): Try<U, E> = when (this) {
        is Success<T> -> Success(transformation(value))
        is Failure<E> -> Failure(error)
    }

    inline fun <F : Throwable> mapErr(transformation: (E) -> F): Try<T, F> = when (this) {
        is Success<T> -> Success(value)
        is Failure<E> -> Failure(transformation(error))
    }

    inline fun onComplete(block: (Try<T, E>) -> Unit): Try<T, E> {
        block(this)
        return this
    }

    inline fun onSuccess(block: (T) -> Unit): Try<T, E> = when (this) {
        is Success<T> -> {
            block(value)
            this
        }
        is Failure<E> -> this
    }

    inline fun onFailure(block: (E) -> Unit): Try<T, E> = when (this) {
        is Success<T> -> this
        is Failure<E> -> {
            block(error)
            this
        }
    }

    companion object {
        fun <T, E : Throwable> fromEither(either: Either<E, T>): Try<T, E> = when (either) {
            is Either.Left -> either.value.failure()
            is Either.Right -> either.value.success()
        }

        inline fun <T> catching(block: () -> T): Try<T, Throwable> =
            try {
                Success(block())
            } catch (err: Throwable) {
                Failure(err)
            }

        inline fun <T, E : Throwable> catchingSpecific(block: () -> T): Try<T, E> =
            try {
                Success(block())
            } catch (err: Throwable) {
                try {
                    @Suppress("UNCHECKED_CAST")
                    (Failure(err as E))
                } catch (_: Throwable) {
                    throw err
                }
            }
    }
}

data class Success<T>(val value: T) : Try<T, Nothing>()
data class Failure<E : Throwable>(val error: E) : Try<Nothing, E>()

fun <T> T.success(): Try<T, Nothing> = Success(this)

fun <E : Throwable> E.failure(): Try<Nothing, E> = Failure(this)

fun <T, E : Throwable, F : E> Try<Try<T, F>, E>.flatten(): Try<T, E> = flatMap { it.mapErr { err -> err } }

fun <T, E : Throwable> Try<T, E>.getOr(default: T): T = when (this) {
    is Success<T> -> value
    is Failure<E> -> default
}

fun <T, E : Throwable> Try<T, E>.or(other: Try<T, E>): Try<T, E> = when (this) {
    is Success<T> -> this
    is Failure<E> -> other
}

fun <T, U, E : Throwable> Try<T, E>.and(other: Try<U, E>): Try<U, E> = when (this) {
    is Success<T> -> other
    is Failure<E> -> Failure(error)
}

inline fun <T, E : Throwable> Try<T, E>.getOrElse(transformation: (E) -> T): T = when (this) {
    is Success<T> -> value
    is Failure<E> -> transformation(error)
}

inline fun <T, U, E : Throwable> Try<T, E>.flatMap(transformation: (T) -> Try<U, E>): Try<U, E> = when (this) {
    is Success<T> -> transformation(value)
    is Failure<E> -> Failure(error)
}

inline fun <T, E : Throwable, F : Throwable> Try<T, E>.flatMapErr(transformation: (E) -> Try<T, F>): Try<T, F> =
    when (this) {
        is Success<T> -> Success(value)
        is Failure<E> -> transformation(error)
    }

inline fun <T, R> R.catching(block: R.() -> T): Try<T, Throwable> =
    try {
        Success(block())
    } catch (err: Throwable) {
        Failure(err)
    }

inline fun <T, E : Throwable, R> R.catchingSpecific(block: R.() -> T): Try<T, E> =
    try {
        Success(block())
    } catch (err: Throwable) {
        try {
            @Suppress("UNCHECKED_CAST")
            (Failure(err as E))
        } catch (_: Throwable) {
            throw err
        }
    }

fun <I : Iterable<Try<T, E>>, T, E : Throwable> I.collect(): Try<Collection<T>, E> {
    return try {
        fold(mutableListOf<T>()) { acc, result ->
            acc.add(result.getOrThrow())
            acc
        }.success()
    } catch (err: Throwable) {
        try {
            @Suppress("UNCHECKED_CAST")
            (Failure(err as E))
        } catch (_: Throwable) {
            throw err
        }
    }
}

fun <I : Sequence<Try<T, E>>, T, E : Throwable> I.collect(): Try<Collection<T>, E> {
    return try {
        fold(mutableListOf<T>()) { acc, result ->
            acc.add(result.getOrThrow())
            acc
        }.success()
    } catch (err: Throwable) {
        try {
            @Suppress("UNCHECKED_CAST")
            (Failure(err as E))
        } catch (_: Throwable) {
            throw err
        }
    }
}

fun <T, E : Throwable> Try<T, E>?.transpose(): Try<T?, E> {
    return when (this) {
        null -> Success(null)
        else -> this.map { it }
    }
}

fun <T, E : Throwable> T?.okOr(err: E): Try<T, E> {
    return if (this == null) {
        Failure(err)
    } else {
        Success(this)
    }
}

fun <T, E : Throwable> T?.okOrElse(err: () -> E): Try<T, E> {
    return if (this == null) {
        Failure(err())
    } else {
        Success(this)
    }
}

fun <T, E : Throwable> Try<T, E>.toEither(): Either<E, T> = when (this) {
    is Success -> value.right()
    is Failure -> error.left()
}
