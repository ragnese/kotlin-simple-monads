@file:Suppress("unused")

package com.gitlab.ragnese

sealed class Either<out T, out U> {
    fun isLeft(): Boolean = when (this) {
        is Left<T> -> true
        else -> false
    }

    fun isRight(): Boolean = when (this) {
        is Right<U> -> true
        else -> false
    }

    fun leftOrNull(): T? = when (this) {
        is Left<T> -> value
        else -> null
    }

    fun rightOrNull(): U? = when (this) {
        is Right<U> -> value
        else -> null
    }

    inline fun <V> fold(ifLeft: (T) -> V, ifRight: (U) -> V): V = when (this) {
        is Left<T> -> ifLeft(value)
        is Right<U> -> ifRight(value)
    }

    inline fun <V> mapLeft(transformation: (T) -> V): Either<V, U> = when (this) {
        is Left<T> -> Left(transformation(value))
        is Right<U> -> this
    }

    inline fun <V> mapRight(transformation: (U) -> V): Either<T, V> = when (this) {
        is Left<T> -> this
        is Right<U> -> Right(transformation(value))
    }

    class Left<out T>(val value: T): Either<T, Nothing>()
    class Right<out U>(val value: U): Either<Nothing, U>()

    companion object {
        fun <T, E: Throwable> fromTry(other: Try<T, E>): Either<E, T> = when (other) {
            is Success -> other.value.right()
            is Failure -> other.error.left()
        }
    }
}

fun <T> T.left(): Either<T, Nothing> = Either.Left(this)
fun <U> U.right(): Either<Nothing, U> = Either.Right(this)

fun <T, U> Either<T, U>.swap(): Either<U, T> = when (this) {
    is Either.Left<T> -> Either.Right(value)
    is Either.Right<U> -> Either.Left(value)
}

fun <T, U> Either<T, U>.getLeftOr(default: T): T = when (this) {
    is Either.Left<T> -> value
    is Either.Right<U> -> default
}

fun <T, U> Either<T, U>.getRightOr(default: U): U = when (this) {
    is Either.Left<T> -> default
    is Either.Right<U> -> value
}

inline fun <T, U > Either<T, U>.getLeftOrElse(transformation: (U) -> T): T = when (this) {
    is Either.Left<T> -> value
    is Either.Right<U> -> transformation(value)
}

inline fun <T, U > Either<T, U>.getRightOrElse(transformation: (T) -> U): U = when (this) {
    is Either.Left<T> -> transformation(value)
    is Either.Right<U> -> value
}

inline fun <T, U, V> Either<T, U>.flatMapLeft(transformation: (T) -> Either<V, U>): Either<V, U> = when (this) {
    is Either.Left<T> -> transformation(value)
    is Either.Right<U> -> Either.Right(value)
}

inline fun <T, U, V> Either<T, U>.flatMapRight(transformation: (U) -> Either<T, V>): Either<T, V> = when (this) {
    is Either.Left<T> -> Either.Left(value)
    is Either.Right<U> -> transformation(value)
}

fun <T: Throwable, U> Either<T, U>.toTry(): Try<U, T> = when (this) {
    is Either.Left<T> -> value.failure()
    is Either.Right<U> -> value.success()
}
