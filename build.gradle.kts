import com.jfrog.bintray.gradle.BintrayExtension

plugins {
    kotlin("jvm") version "1.3.72"
    `maven-publish`
    signing
    id("com.jfrog.bintray") version "1.8.5"
}

group = "com.gitlab.ragnese"
version = "0.1.1"
description = "Try, Option, and Either monads for Kotlin."

ext {
    set("websiteUrl", "https://gitlab.com/ragnese/kotlin-simple-monads")
    set("issueTrackerUrl", "https://gitlab.com/ragnese/kotlin-simple-monads/issues")
    set("vcsUrl", "https://gitlab.com/ragnese/kotlin-simple-monads.git")
    set("licenseUrl", "https://gitlab.com/ragnese/kotlin-simple-monads/blob/master/LICENSE")
    set("license", "Apache-2.0")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

val sourcesJar = tasks.register<Jar>("sourcesJar") {
    from(sourceSets["main"].allSource)
    archiveClassifier.set("sources")
}

val javadocJar = tasks.register<Jar>("javadocJar") {
    dependsOn(JavaPlugin.JAVADOC_TASK_NAME)
    archiveClassifier.set("javadoc")
    from(tasks["javadoc"])
}

val bintrayUser: String? by project
val bintrayKey: String? by project
val bintrayPassphrase: String? by project

// For the bintray{} closure
fun BintrayExtension.pkg(configure: BintrayExtension.PackageConfig.() -> Unit) {
    pkg(delegateClosureOf(configure))
}

// For the bintray{} closure
fun BintrayExtension.PackageConfig.version(configure: BintrayExtension.VersionConfig.() -> Unit) {
    version(delegateClosureOf(configure))
}

// For the bintray{} closure
fun BintrayExtension.VersionConfig.gpg(configure: BintrayExtension.GpgConfig.() -> Unit) {
    gpg(delegateClosureOf(configure))
}

bintray {
    user = bintrayUser
    key = bintrayKey
    setPublications("maven")

    pkg {
        repo = "maven"
        name = project.name
        websiteUrl = project.ext["websiteUrl"] as String
        issueTrackerUrl = project.ext["issueTrackerUrl"] as String
        vcsUrl = project.ext["vcsUrl"] as String
        setLicenses(project.ext["license"] as String)

        version {
            name = project.version as String
            gpg {
                sign = true
                passphrase = bintrayPassphrase
            }
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            groupId = project.group as String
            artifactId = project.name
            version = project.version as String
            artifact(sourcesJar.get())
            artifact(javadocJar.get())
            pom {
                name.set(project.name)
                description.set(project.description)
                url.set(project.ext["websiteUrl"] as String)
                licenses {
                    license {
                        name.set(project.ext["license"] as String)
                        url.set(project.ext["licenseUrl"] as String)
                    }
                }
                developers {
                    developer {
                        id.set("ragnese")
                        name.set("Rob Agnese")
                        email.set("ragnese@protonmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://gitlab.com/ragnese/kotlin-simple-monads.git")
                    developerConnection.set("scm:git:git@gitlab.com/ragnese/kotlin-simple-monads.git")
                    url.set(project.ext["vcsUrl"] as String)
                }
            }
        }
    }
}

signing {
    useGpgCmd()
    sign(configurations.archives.get())
}
