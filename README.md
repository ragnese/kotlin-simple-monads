# Documentation, tests, etc, coming soon...

# To Use
## Maven
```
<dependency>
  <groupId>com.gitlab.ragnese</groupId>
  <artifactId>kotlin-simple-monads</artifactId>
  <version>0.1.1</version>
  <type>pom</type>
</dependency>
```
## Gradle
```
implementation 'com.gitlab.ragnese:kotlin-simple-monads:0.1.1'
```